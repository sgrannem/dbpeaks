from setuptools import setup, find_packages

setup(
    name='DBPeaks',
    version='0.0.3',
    description='A tool for identifying differentially bound peaks in CLIP/CRAC data',
    author='Sander Granneman',
    author_email='Sander.Granneman@ed.ac.uk',
    packages=find_packages(),
    install_requires=[
        'pybedtools',
        'numpy',
        'pandas',
        'pydeseq2',
        'pyCRAC',
        'concurrent-futures; python_version < "3.2"'
    ],
    entry_points={
        'console_scripts': [
            'dbpeaks=DBPeaks:main',  # Assuming 'main' is the entry function in 'DBPeaks.py'
        ],
    },
    classifiers=[
        'Development Status :: 3 - Beta',
        'Intended Audience :: Science/Research',
        'Topic :: Scientific/Engineering :: Bio-Informatics',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9',
    ],
    python_requires='>=3.6',

    scripts=['DBPeaks.py'],

)
