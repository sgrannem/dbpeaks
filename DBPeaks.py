#/usr/bin/python

__author__		= "Sander Granneman"
__copyright__	= "Copyright 2024"
__version__		= "0.0.3"
__credits__		= ["Sander Granneman"]
__maintainer__	= "Sander Granneman"
__email__		= "Sander.Granneman@ed.ac.uk"
__status__		= "beta"

import os
import re
import csv
import sys
import pybedtools
import argparse
import subprocess
import numpy as np
import pandas as pd

from concurrent.futures import ThreadPoolExecutor
from collections import defaultdict
from pydeseq2.dds import DeseqDataSet
from pydeseq2.default_inference import DefaultInference
from pydeseq2.ds import DeseqStats
from pyCRAC.Parsers import GTF2
from pyCRAC.Methods import numpy_overlap
from pyCRAC.Classes.NGSFormatWriters import NGSFileWriter

def getGeneIDs(string):
    """Finds all the gene_ids in a given string"""
    gene_ids = list(set(re.findall(r'gene_id\s+"([^"]+)"', string)))
    return gene_ids

def getGeneNames(string):
    """Finds all the gene_names in a given string"""
    gene_names = list(set(re.findall(r'gene_name\s+"([^"]+)"', string)))
    return gene_names

def rowToGTF(row):
    """ Returns the results from the mergeGTFfiles function as a GTF file string """
    return f"{row['chrom']}\tcluster\tinterval\t{row['start']}\t{row['end']}\t.\t{row['name']}\t.\tgene_id \"{row['gene_ids']}\"; gene_name \"{row['gene_names']}\";"

def mergeGTFintervals(gtf_files, reproducibility=0.9, output_file_name=None):
    """Concatenates the files and then uses pybedtools to find intervals/peaks that are found in all replicates.
    The user can decide whether only some replicates should contain the peaks or all. This can be done by setting
    the 'reproducibility' variable in the function, which is set to 0.9 (i.e., 90% by default).
    """

    if not output_file_name:
        output_file_name = "merged.gtf"

    if not os.path.exists(output_file_name):
        ### Concatenating the files and storing them in a pandas dataframe:
        data_frames = [pd.read_csv(i, sep='\t', comment='#', index_col=None, header=None) for i in gtf_files]
        merged_data = pd.concat(data_frames, ignore_index=True)

        ### Sorting the merged data:
        merged_data = merged_data.sort_values(by=[0, 1, 2, 3, 8])

        ### Loading the dataframe into pybedtools:
        bedtools_data = pybedtools.BedTool.from_dataframe(merged_data)

        ### Merging the data using bedtools:
        bedtools_data_merged = bedtools_data.merge(s=True, c=[7, 9], o='collapse', delim='')

        ### Converting the results back into a dataframe:
        bedtools_data_merged = pybedtools.BedTool.to_dataframe(bedtools_data_merged)

        ### Now only keeping peaks that were found in multiple replicates, based on the threshold:
        number_of_reps = len(gtf_files)    
        must_be_seen_in_reps = number_of_reps * float(reproducibility)

        ### Filter rows where the length of the string in the 'name' column exceeds the threshold:
        bedtools_data_merged = bedtools_data_merged[bedtools_data_merged['name'].apply(len) >= must_be_seen_in_reps]

        ### Keep only the first character of the strand column:
        bedtools_data_merged['name'] = bedtools_data_merged['name'].str[0]  # Keep only the first character

        ### Now extracting the gene_ids and gene_names, only keeping the unique ones:
        bedtools_data_merged['gene_ids'] = bedtools_data_merged['score'].apply((getGeneIDs))
        bedtools_data_merged['gene_ids'] = bedtools_data_merged['gene_ids'].apply(lambda x: '|'.join(x))
        
        bedtools_data_merged['gene_names'] = bedtools_data_merged['score'].apply((getGeneNames))
        bedtools_data_merged['gene_names'] = bedtools_data_merged['gene_names'].apply(lambda x: '|'.join(x))

        ### Now dropping the score column
        bedtools_data_merged.drop(columns=['score'],inplace=True)

        ### Resetting the index:
        bedtools_data_merged = bedtools_data_merged.reset_index()

        gtf_file_lines = list()
        gtf_file_lines = bedtools_data_merged.apply(rowToGTF,axis=1).to_list()

        if gtf_file_lines:
            outfile = open(output_file_name,'w')
            outfile.write("##gff-version 2\n")
            
            for i in gtf_file_lines:
                outfile.write(f"{i}\n")
            outfile.close()
            return True

        else:
            sys.stderr.write("ERROR! The data could not be merged!\n")
            return False
    else:
        sys.stderr.write(f"\tOutput file {output_file_name} already exists!\n")
        return False

def countReadsBam(bam_files,gtf_annotation_file,no_cpus=1,output_dir="bam_read_counts",blocks=False):
    """ Runs the pyReadCounter analyses on the bam files"""

    ### Creating the directory where the results will be stored:  
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    def run_command(file_name,gtf_file,outfile_name):
        cmd = [
                "pyReadCounters.py",
                "-f",
                file_name,
                "--file_type",
                "sam",
                "--gtf",
                gtf_file,
                "-v",
                "--gtffile",
                "-o",
                outfile_name
            ]

        if blocks:
            cmd.extend(["--blocks",
                        "--mutations",
                        "nomuts"
                       ]
                      )
        subprocess.run(cmd)


    data_type = "reads"
    if blocks:
        data_type = "cDNAs"

    ### Running pyReadCounters over multiple processors.
    with ThreadPoolExecutor(no_cpus) as executor:
        futures = []
        for file_name in bam_files:
            basename = getFileBaseName(file_name)
            # The name of the output file path that should be submitted to pyReadCounters:
            output_file_name = f"{output_dir}/{basename}"
            # The name of the output file path produced by pyReadCounters:
            output_file_path = f"{output_file_name}_count_output_{data_type}.gtf"
            # If the file already exists, don't overwrite it.
            if not os.path.exists(output_file_path):
                future = executor.submit(run_command,file_name,gtf_annotation_file,output_file_name)
                futures.append(future)
            else:
                sys.stderr.write(f"\tOutput file {output_file_path} already exists!\n")

    # Wait for all commands to complete
    if futures:
        for future in futures:
            future.result()

    return True

def countReadsGTF(bam_files,gtf_annotation_file,no_cpus=1,output_dir="peak_hittables",blocks=False):
    """ Runs the pyReadCounter analyses on the bam files"""
    
    ### Creating the directory where the results will be stored:    
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
    
    def run_command(file_name,gtf_file,outfile_name):
        cmd = [
                "pyReadCounters.py",
                "-f",
                file_name,
                "--file_type",
                "sam",
                "--gtf",
                gtf_file,
                "-v",
                "--hittable",
                "-o",
                outfile_name
            ]

        if blocks:
            cmd.extend(["--blocks",
                        "--mutations",
                        "nomuts"
                       ]
                      )
        subprocess.run(cmd)

    data_type = "reads"
    if blocks:
        data_type = "cDNAs"
    
    ### Running pyReadCounters over multiple processors.
    with ThreadPoolExecutor(no_cpus) as executor:
        futures = []
        for file_name in bam_files:
            basename = getFileBaseName(file_name)
            # The name of the output file path that should be submitted to pyReadCounters:
            output_file_name = f"{output_dir}/{basename}"
            # The name of the output file path produced by pyReadCounters:
            output_file_path = f"{output_file_name}_hittable_{data_type}.txt"
            # If the file already exists, don't overwrite it.
            if not os.path.exists(output_file_path):
                future = executor.submit(run_command, file_name, gtf_annotation_file, output_file_name)
                futures.append(future)
            else:
                sys.stderr.write(f"\tOutput file {output_file_path} already exists!\n")

    # Wait for all commands to complete
    if futures:
        for future in futures:
            future.result()

def getPeaks(gtf_files,gtf_annotation_file,chromosome_file,no_cpus=1,min_peak_height=5,min_fdr=0.05,output_dir="peak_gtf_files"):
    """ Runs pyCalculateFDRs to get peaks enriched in the data relative to random control dataset """
    
    ### Creating the directory where the results will be stored:   
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    def run_command(file_name,gtf_annotation_file,chromosome_file,min_peak_height,min_fdr,output_file_name):
        cmd = [
                "pyCalculateFDRs.py",
                "-f",
                file_name,
                "--gtf",
                gtf_annotation_file,
                "-c",
                chromosome_file,
                "--min",
                str(min_peak_height),
                "-m",
                str(min_fdr),
                "-v",
                "-o",
                output_file_name
            ]
        subprocess.run(cmd)

    ### Running pyCalculateFDRs over multiple processors.
    with ThreadPoolExecutor(no_cpus) as executor:
        futures = []
        for file_name in gtf_files:
            basename = getFileBaseName(file_name)
            output_file_name = f"{output_dir}/{basename}_FDR_{str(min_fdr)}_min_peak_height_{str(min_peak_height)}.gtf"
            if not os.path.exists(output_file_name):
                future = executor.submit(run_command,
                                         file_name,
                                         gtf_annotation_file,
                                         chromosome_file,
                                         min_peak_height,
                                         min_fdr,
                                         output_file_name)
                futures.append(future)
            else:
                sys.stderr.write(f"\tOutput file {output_file_name} already exists!\n")

    # Wait for all commands to complete
    if futures:
        for future in futures:
            future.result()

def filterPeaks(peak_gtf_files,by=None,output_dir="filtered_peak_files"):
    """ Filters all the peaks in gtf files by a specific threshold. This
     threshold could be the mean, median or mean plus one standard devation 
    (mean_plus_std) peak heights. Default is mean. """

    ### Creating the directory where the results will be stored:  
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    peak_heights = defaultdict(list)
    data_thresholds = defaultdict(float)

    for file_name in peak_gtf_files:
        with open(file_name,'r') as infile:
            for line in infile:
                if not line.startswith("#"):
                    fld = line.strip().split('\t')
                    peak_height = float(fld[5])
                    peak_heights[file_name].append(peak_height)

    ### Calculating thresholds:
    data_thresholds = defaultdict(float)

    for file_name, value in peak_heights.items():
        ### Calculate the mean and use mean+stdev as the threshold:
        threshold = float()

        if by == 'mean':
            threshold = np.mean(value)
        elif by == 'median':
            threshold = np.median(value)
        elif by == 'mean_plus_std':
            threshold = np.mean(value) + np.std(value)
        elif by == "None":
            threshold = 0
        else:
            sys.stderr.write("ERROR! Cannot figure out how you want to filter the peaks! Please use mean, median, mean_plus_std, or None\n")
            threshold = 0
            
        data_thresholds[file_name] = threshold

    ### Using these thresholds to remove peaks
    for file_name, value in peak_heights.items():
        basename = getFileBaseName(file_name)
        output_file_name = f"{output_dir}/{basename}_filtered_by_{by}_threshold.gtf"
        if not os.path.exists(output_file_name) and os.path.exists(file_name):
            outfile = open(output_file_name,"w")
            threshold = data_thresholds[file_name]
            with open(file_name) as peak_file:
                for line in peak_file:
                    if not line.startswith("#"):
                        fld = line.strip().split('\t')
                        peak_height = float(fld[5])
                        if peak_height >= threshold:
                            outfile.write(line)
                    else:
                        outfile.write(line)
            outfile.close()
        else:
            sys.stderr.write(f"\tOutput file {output_file_name} already exists!\n")

    return True
        
def adjustPeakWidths(peak_gtf_files,chromosome_file,min_width=20,no_cpus=1,output_dir = "filtered_peak_files"):
    """ Normalises the peak widths to a minimum length. """

    ### Creating the directory where the results will be stored: 
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    def run_command(file_name,chromosome_file,outfile_name=None,min_width=20):
        cmd = [
                "pyNormalizeIntervalLengths.py",
                "-f",
                file_name,
                "-c",
                chromosome_file,
                "--min",
                str(min_width),
                "-o",
                outfile_name
            ]
        subprocess.run(cmd)

    ### Running pyNormalizeIntervalLengths over multiple processors.
    with ThreadPoolExecutor(no_cpus) as executor:
        futures = []
        for file_name in peak_gtf_files:
            basename = getFileBaseName(file_name)
            output_file_name = f"{output_dir}/{basename}_min_width_{str(min_width)}.gtf"
            if not os.path.exists(output_file_name):
                future = executor.submit(run_command,file_name,chromosome_file,output_file_name,min_width)
                futures.append(future)
            else:
                sys.stderr.write(f"\tOutput file {output_file_name} already exists!\n")
    
    # Wait for all commands to complete
    if futures:
        for future in futures:
            future.result()

    return True

def numberPeaks(peak_gtf_files,output_dir="filtered_peak_files"):
    """ Gives each peak a unique number to avoid a scenario where peaks end up having the same names, 
    which can cause problems with downstream data analysis steps. """

    ### Creating the directory where the results will be stored:
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    for gtf_file in peak_gtf_files:
        basename = getFileBaseName(gtf_file)
        output_file_name = f"{output_dir}/{basename}_numbered.gtf"
        if not os.path.exists(output_file_name):
            outfile = open(output_file_name,"w")
            with open(gtf_file,'r') as infile:
                peak_number = 1
                for line in infile:
                    if not line.startswith("#"):
                        try:
                            gene_id = re.search('gene_id \"([\(\)a-zA-Z_0-9-,\'|]+?)\";',line).group(1)
                            gene_name = re.search('gene_name \"([\(\)a-zA-Z_0-9-,\'|]+?)\";',line).group(1)

                            line = line.replace(f'gene_id \"{gene_id}\";',f'gene_id \"{gene_id}_peak_{peak_number}\";')
                            line = line.replace(f'gene_name \"{gene_name}\";',f'gene_name \"{gene_name}_peak_{peak_number}\";')
                            peak_number += 1
                            outfile.write(line)
                        except:
                            sys.stderr.write(line)
                    else:
                        outfile.write(line)
                outfile.close()
            return True
        else:
            sys.stderr.write(f"\tOutput file {output_file_name} already exists!\n")
            return False

def getFileBaseName(file_name):
    """ Returns the file basename without extension. """
    return os.path.splitext(os.path.basename(file_name))[0]

def mergeHittables(hittables,outfile_name="merged_hittables.txt"):
    """ Merges pyReadCounters hittables. """

    genes = defaultdict(set)
    data  = dict()
    feature = str()
    columns = [0,1]
    sumofdata = defaultdict(float)
    mappedreadsdata = defaultdict()

    if not os.path.exists(outfile_name):
        for i in hittables:
            data[i] = defaultdict(lambda: defaultdict(int))
            mappedreadsdata[i] = defaultdict(float)
            with open(i,"r") as infile:
                mappedreads = 0
                for line in infile:
                    if line.startswith("##"):
                        feature = line.strip().split()[1]
                    elif line.startswith("# total number of paired"):
                        mappedreads += int(line.strip().split("\t")[-1]) 
                    elif line.startswith("# total number of single"):
                        mappedreads += int(line.strip().split("\t")[-1]) 
                    elif re.search("[A-Za-z0-9]",line[0] ):
                        Fld = line.strip().split("\t")
                        gene,hits = Fld[columns[0]],Fld[columns[1]]
                        data[i][feature][gene] = float(hits)
                        genes[feature].add(gene)
                mappedreadsdata[i] = mappedreads             

        outfile = open(outfile_name,"w")

        outfile.write("# gene\t%s\n" % ("\t".join([getFileBaseName(i) for i in hittables])))
        outfile.write("# total mapped reads:\t%s\n" % ("\t".join([str(mappedreadsdata[i]) for i in hittables])))
        for feature in sorted(genes):
            for i in hittables:
                sumofdata[i] = sum([data[i][feature][j] for j in genes[feature]])
            sumoffeaturehits = "\t".join([str(sumofdata[x]) for x in hittables])
            outfile.write("\n## %s\t%s\n" % (feature,sumoffeaturehits))
            for gene in sorted(list(genes[feature])):
                hitstring = "\t".join([str(data[i][feature][gene]) for i in hittables])
                outfile.write("%s\t%s\n" % (gene,hitstring))
        return True
    else:
        sys.stderr.write(f"\tOutput file {outfile_name} already exists!\n")
        return False

def runDESeq(merged_hittable,conditions,no_cpus=1,output_dir="DESeq2_results"):
    """ Runs the DESeq2 analyses on the samples. Returns the results in a text file. """

    ### Creating the directory where the results will be stored:
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    ### Defining the GTF file:
    basename = getFileBaseName(merged_hittable)
    outfile_name = f"{output_dir}/{basename}_DESeq2_results.txt"

    if not os.path.exists(outfile_name):
        ### opening the merged hittable file:
        data = pd.read_csv(merged_hittable,comment="#",sep="\t",header=None,index_col=None)
        columns = ["gene"]
        columns.extend(conditions)

        data.columns = columns
        data.set_index('gene',inplace=True)

        ### Creating a DataFrame with column data:
        colData = pd.DataFrame(data.columns, columns=['gene'])
        colData['Conditions'] = conditions

        ### Changing the index so that the first column of both 
        ### countData and colData are the same:
        colData = colData.set_index('gene')

        ### Starting DESeq2
        inference = DefaultInference(n_cpus=no_cpus)

        dds = DeseqDataSet(
            counts=data.T,
            metadata=colData,
            design_factors="Conditions",
            refit_cooks=True,
            inference=inference,
        )

        dds.deseq2()
        stat_res = DeseqStats(dds, inference=inference)

        ### Getting the final results:
        stat_res.summary()
        final_results = stat_res.results_df

        ### Storing the final results:
        final_results.to_csv(outfile_name,sep="\t")
        return True
    else:
        sys.stderr.write(f"\tOutput file {outfile_name} already exists!\n")
        return False

def addFoldChangeToGTF(deseq_results,merged_peaks,output_dir="./"):
    """ Adds the log2-fold changes calculated be DESeq2 to the peak gtf file. """
    
    ### Setting the output file names:
    peak_outfile_name = f"{output_dir}/{getFileBaseName(merged_peaks)}_with_padj.gtf"

    ### If the output file already exist, then don't overwrite:
    if not os.path.exists(peak_outfile_name):
        ### Creating the output file:
        outfile = open(peak_outfile_name,"w")
        ### loading the input files: 
        deseq_data = pd.read_csv(deseq_results,comment="#",sep="\t",index_col=None,header=0)
        
        ### Opening the peak data:
        with open(merged_peaks,"r") as peak_file:
            for line in peak_file:
                if not line.startswith("#"):
                    gene_name = re.search('gene_name \"([\(\)a-zA-Z_0-9-,\'|]+?)\";',line).group(1)
                    if gene_name in deseq_data["gene"].values:
                        log2fold_change = deseq_data.loc[deseq_data["gene"] == gene_name,"log2FoldChange"].values[0]
                        p_value = deseq_data.loc[deseq_data["gene"] == gene_name,"padj"].values[0]
                        line = f"{line.strip()} log2foldchange \"{log2fold_change}\"; padj \"{p_value}\";\n"                   
                    else:
                        line = f"{line.strip()} log2foldchange \"unknown\"; padj \"unknown\";\n" 
                outfile.write(line)
        outfile.close()
        return True
    else:
        sys.stderr.write(f"\tOutput file {peak_outfile_name} already exist!\n")
        return False

def getSignificantPeaks(deseq_results,merged_peaks,fdr_threshold=0.05,output_dir="DESeq2_results"):
    """ Extracts the significantly DE peaks from the merged peak GTF file """

    ### Setting the output file names:
    deseq_outfile_name = f"{output_dir}/{getFileBaseName(deseq_results)}_FDR_{str(fdr_threshold)}.txt"
    peak_outfile_name = f"{output_dir}/{getFileBaseName(merged_peaks)}_FDR_{str(fdr_threshold)}.gtf"

    ### If the output files already exist, then don't overwrite:
    if not os.path.exists(peak_outfile_name):
        ### loading the input files: 
        deseq_data = pd.read_csv(deseq_results,comment="#",sep="\t",index_col=None,header=0)

        ### Filter the DESeq2 results:
        deseq_data = deseq_data.loc[deseq_data['padj'] <= fdr_threshold]
        deseq_data.to_csv(deseq_outfile_name,sep="\t",index=None)
        
        ### DE genes:
        de_genes = list(deseq_data[deseq_data.columns[0]])
        
        ### Opening the peak data:
        peak_data = pd.read_csv(merged_peaks,index_col=None,header=None,comment="#",sep="\t")
        peak_data.columns = ['chrom','source','feature','start','end','score','strand','frame','annotations']
        
        ### Making a seperate gene_name colum:
        peak_data['gene_name'] = peak_data['annotations'].str.extract(r'gene_name \"(.*?)\"', expand=False)

        ### Filter rows based on the list of gene names
        filtered_peak_data = peak_data[peak_data['gene_name'].isin(de_genes)]
        filtered_peak_data = filtered_peak_data.drop(columns=['gene_name'])
        filtered_peak_data.to_csv(peak_outfile_name,sep="\t",header=False,index=False,quoting=csv.QUOTE_NONE)

        return True
    else:
        sys.stderr.write(f"\tOutput files {peak_outfile_name} and {deseq_outfile_name} already exist!\n")
        return False


def main(): 
    parser = argparse.ArgumentParser(usage="usage: %(prog)s [options]", description="A tool for identifying differential RNA-binding sites in CLIP/CRAC datasets")
    
    files = parser.add_argument_group("File input options")
    files.add_argument("--samples", dest="samples", nargs="*", metavar="clip_samples.bam", 
                       help="Paths to the bam files containing the replicate clip samples you want to compare", default=None)
    files.add_argument("--controls", dest="controls", nargs="*", metavar="control_clip_samples.bam", 
                       help="Paths to bam files containing replicate control clip samples.", default=None)
    files.add_argument("-c", "--chromfile", dest="chromfile", type=str,
                       help="Location of the chromosome info file. This file should have two columns: \
                        first column is the names of the chromosomes, second column is length of the chromosomes.", default=None)
    files.add_argument("--gtf", dest="gtf_annotation", type=str, metavar="yeast.gtf", 
                       help="Path to GTF anotation file for your organism containing gene location information.", default=None)
    files.add_argument("-j","--jobname",dest="jobname",type=str,metavar="WT_vs_mutant",
                       help="provide a name for the job. Default = WT_vs_mutant")
    #files.add_argument("--log", dest="log", help="To print all the command lines used during the run to the 'command_lines.txt' file",
    #                   action="store_true",default=False)

    peaks = parser.add_argument_group("Peak calling settings")
    peaks.add_argument("-m", "--minfdr", dest="minfdr", type=float, metavar="0.05", 
                       help="To set a minimal FDR threshold for filtering interval data. Default is 0.05", default=0.05)
    peaks.add_argument("--padj", dest="padj", metavar="0.05", type=float,
                       help="DESeq2 threshold for calling a DE peak. Default is 0.05", default=0.05)
    peaks.add_argument("--min", dest="min", metavar="5", 
                       help="to set a minimal read coverages for a region. Regions with coverage less than minimum will be ignoredve an FDR of zero", type=int, default=1)
    peaks.add_argument("--blocks", dest="blocks", help="Add this flag if you want to consider reads with identical mapping coordinates once, regardless of sequence",
                       action="store_true",default=False)
    peaks.add_argument("--iterations", dest="iterations", metavar="100", type=int, 
                       help="to set the number of iterations for randomization of read coordinates. Default=100", default=100)
    peaks.add_argument("-r","--rep",dest="reproducibility",metavar="90", type=float, 
                       help="To set in what percentage of the replicates the peak should be detected. Default=100", default=100.0)
    peaks.add_argument("--filter",dest="filter_peak_height", metavar="mean", type=str,
                       help="To filter the peaks in gtf files by a specific threshold. \
                       Options are mean, median or mean plus one standard devation (mean_plus_std) peak heights. Default is no filtering.",default="None",choices=["mean","median","mean_plus_std","None"])
    peaks.add_argument("--min_peak_width",dest="min_peak_width",type=int,metavar="20"
                       ,help="To set the minimum width of a called peak. Default = 20",default=20)
        
    log = parser.add_argument_group("Logging options")
    log.add_argument("-v", "--verbose", action="store_true", help="to print status messages to a log file", default=False)		
    
    cpu = parser.add_argument_group("Number of CPUs needed for the analyses")
    cpu.add_argument("--cpu", dest="cpu", type=int, metavar="12", help="The number of processors you want to use for the analyses. Default = 1", default=1)	 
    
    args = parser.parse_args()

    ### Setting key parameters;
    
    data_type = "reads"
    
    if args.blocks:
        data_type = "cDNAs"
        

    ### Running the code:

    # Making the directory where the results files are stored:
    storage_dir = f"{args.jobname}"
    if not os.path.exists(storage_dir):
        os.makedirs(storage_dir)

    # Getting read counts for all the bam files:
    if args.verbose:
        sys.stdout.write("### Getting gene counts from sample bam files....\n")

    all_bam_files = list()
    all_bam_files.extend(args.samples)
    all_bam_files.extend(args.controls)
    
    countReadsBam(all_bam_files,
                  args.gtf_annotation,
                  no_cpus=args.cpu,
                  output_dir=f"{storage_dir}/bam_read_counts"
                  )

    # Getting all the peak_files:
    if args.verbose:
        sys.stdout.write("### Finding peaks in the sample files....\n")

    file_basenames = [getFileBaseName(i) for i in all_bam_files]
    
    all_read_counters_files = [f"{storage_dir}/bam_read_counts/{i}_count_output_{data_type}.gtf" for i in file_basenames]

        
    getPeaks(all_read_counters_files,
             args.gtf_annotation,
             args.chromfile,
             no_cpus=args.cpu,
             min_peak_height=args.min,
             min_fdr=args.minfdr,
             output_dir=f"{storage_dir}/peak_gtf_files"
            )
    
    # Filtering the peaks by peak height:
    if args.verbose:
        sys.stdout.write(f"### Filtering the peaks by {args.filter_peak_height} values of peak heights....\n")

    read_counters_file_basenames = [getFileBaseName(i) for i in all_read_counters_files]
    peak_gtf_files = [f"{storage_dir}/peak_gtf_files/{i}_FDR_{str(args.minfdr)}_min_peak_height_{str(args.min)}.gtf" \
                      for i in read_counters_file_basenames]
    
    filterPeaks(peak_gtf_files,
                by=args.filter_peak_height,
                output_dir=f"{storage_dir}/filtered_peak_files"
                )

    # Setting minimum peak widths:
    if args.verbose:
        sys.stdout.write(f"### Adjusting peak widths to a minimum of {args.min_peak_width}....\n")

    file_basenames = [getFileBaseName(i) for i in peak_gtf_files]
    filtered_peak_files = [f"{storage_dir}/filtered_peak_files/{i}_filtered_by_{args.filter_peak_height}_threshold.gtf" \
                           for i in file_basenames]
    
    adjustPeakWidths(filtered_peak_files,
                     args.chromfile,
                     min_width=args.min_peak_width,
                     no_cpus=args.cpu,
                     output_dir=f"{storage_dir}/filtered_peak_files"
                     )

    # Merging the peak intervals for the sample files:
    if args.verbose:
        sys.stdout.write("### Merging peak intervals...\n")

    file_basenames = [getFileBaseName(i) for i in filtered_peak_files]
    filtered_peak_files = [f"{storage_dir}/filtered_peak_files/{i}_min_width_{args.min_peak_width}.gtf" for i in file_basenames]
    reproducibility = args.reproducibility/100.0
    merged_peak_output_file_name = f"{storage_dir}/filtered_peak_files/{args.jobname}_merged_peaks.gtf"

    mergeGTFintervals(filtered_peak_files,
                      reproducibility,
                      merged_peak_output_file_name,
                      )

    # Numbering the peaks:
    if args.verbose:
        sys.stdout.write("### Numbering the peaks....\n")

    numberPeaks([merged_peak_output_file_name],
                output_dir=f"{storage_dir}/filtered_peak_files"
                )

    # Performing pyReadCounters analysis on the bam files using the new GTF file containing the numbered peaks:
    if args.verbose:
        sys.stdout.write("### Counting peak coverage for each sample and control file....\n")
    
    annotation_file = f"{os.path.splitext(merged_peak_output_file_name)[0]}_numbered.gtf"

    # Making sure the input file actually exists!:
    if os.path.exists(annotation_file):
        
        all_bam_files = list()
        all_bam_files.extend(args.samples)
        all_bam_files.extend(args.controls)

        countReadsGTF(all_bam_files,
                    gtf_annotation_file=annotation_file,
                    no_cpus=args.cpu,
                    output_dir=f"{storage_dir}/peak_hittables"
                    )
    else:
        sys.stderr.write(f"The file {os.path.basename(annotation_file)} already exists!\n")    
    
    # Merging the hittables:
    if args.verbose:
        sys.stdout.write("Merging the peak read coverage hit tables.\n")

    sample_file_base_names = [getFileBaseName(i) for i in args.samples]
    control_file_base_names = [getFileBaseName(i) for i in args.controls]

    sample_file_hittables = [f"{storage_dir}/peak_hittables/{i}_hittable_{data_type}.txt" for i in sample_file_base_names]
    control_file_hittables = [f"{storage_dir}/peak_hittables/{i}_hittable_{data_type}.txt" for i in control_file_base_names]

    tables_to_merge = list()
    tables_to_merge.extend(sample_file_hittables)
    tables_to_merge.extend(control_file_hittables)
    hittable_name = f"{storage_dir}/peak_hittables/{args.jobname}_merged_hittables.txt"

    if not os.path.exists(hittable_name):
        mergeHittables(tables_to_merge,hittable_name)
    else:
        sys.stderr.write(f"\tOutput file {os.path.basename(hittable_name)} already exists!\n")

    # Running the DESeq2 analyses:
    # Setting the testing conditions:
    if args.verbose:
        sys.stdout.write("### Running the DESeq2 analyses....\n")

    conditions = list()
    conditions.extend(len(args.samples)*["WT"])
    conditions.extend(len(args.controls)*["control"])

    # Order of Conditions: The order in which conditions are specified affects the reference level in DESeq2. 
    # In our case, "WT" samples are listed first in the conditions list and then "Control samples"
    # DESeq2 will treat "WT" as the reference level by default (assuming no other specifications are made to alter this). 
    # This means:
    # POSITIVE log2FC: Indicates higher expression in "control" relative to "WT".
    # NEGATIVE log2FC: Indicates higher expression in "WT" relative to "control".

    runDESeq(hittable_name,
             conditions,
             no_cpus=args.cpu,
             output_dir=f"{storage_dir}/DESeq2_results"
             )

    # Adding log2-fold changes to the peak GTF file:
    deseq_table_name = f"{storage_dir}/DESeq2_results/{getFileBaseName(hittable_name)}_DESeq2_results.txt"

    addFoldChangeToGTF(deseq_table_name,
                       annotation_file,
                       output_dir=f"{storage_dir}/DESeq2_results"
                       )

    # Extracting the significant peaks
    annotation_file_with_padj = f"{storage_dir}/DESeq2_results/{getFileBaseName(annotation_file)}_with_padj.gtf"
    
    if args.verbose:
        sys.stdout.write("#### Extracting significant peaks....\n")

    getSignificantPeaks(deseq_table_name,
                        annotation_file_with_padj,
                        fdr_threshold=args.padj,
                        output_dir=f"{storage_dir}/DESeq2_results"
                        )

if __name__ == "__main__":
    main()